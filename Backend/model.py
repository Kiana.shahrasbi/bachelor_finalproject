from exts import db


#########
#class Recipe:
    #id:int primary key
    #title:str
    #description:str (text)
#########

class Recipe(db.Model):
    id=db.Column(db.Integer(),primary_key=True)
    title=db.Column(db.String(),nullable=False)
    description=db.Column(db.Text(),nullable=False)

    def __repr__(self):
        return f"<Recipe{self.title}>"

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self,title,description):
        self.title=title
        self.description=description
        db.session.commit()


#########
#class user:
    #id:integer
    #username:string
    #email:string
    #password:string
#########

class User(db.Model):
    username=db.Column(db.String(25),nullable=False,unique=True,primary_key=True)
    email=db.Column(db.String(80),nullable=False)
    password=db.Column(db.Text(),nullable=False)

    def __repr__(self):
        return f"<User{self.username}>"

    def save(self):
        db.session.add(self)
        db.session.commit()


class testnextgpa(db.Model):
    id=db.Column(db.Integer(),nullable=False,unique=True,primary_key=True)
    gender=db.Column(db.Text())
    residence=db.Column(db.Text())
    birthdate=db.Column(db.Integer())
    entering=db.Column(db.Integer())
    entering_type=db.Column(db.Integer())
    F_obtained_units=db.Column(db.Integer())
    F_pass=db.Column(db.Integer())
    F_fail=db.Column(db.Integer())
    F_gpa=db.Column(db.Float())
    TF_gpa=db.Column(db.Float())
    S_obtained_units = db.Column(db.Integer())
    S_pass = db.Column(db.Integer())
    S_fail = db.Column(db.Integer())
    S_gpa = db.Column(db.Float())
    TS_gpa = db.Column(db.Float())
    Th_obtained_units = db.Column(db.Integer())
    obtained_units_till_Th = db.Column(db.Integer())
    f_o = db.Column(db.Float())
    p_o = db.Column(db.Float())
    terms_last=db.Column(db.Integer())
    m_t = db.Column(db.Float())

    def __repr__(self):
        return f"<TestNextGpa{self.id}>"

    def save(self):
        db.session.add(self)
        db.session.commit()




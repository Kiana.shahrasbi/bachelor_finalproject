import joblib as joblib
import numpy as np
from flask_restx import Resource,Namespace,fields
from flask_jwt_extended import JWTManager,create_access_token,create_refresh_token,get_jwt_identity,jwt_required
from flask import Flask, request, json, jsonify,make_response

gpaPredict_ns=Namespace("prediction",description="A namespace for prediction")

gpaPredict_model=gpaPredict_ns.model(
    "prediction",{
        "pass units":fields.Float(),
        "fail units":fields.Float(),
        "mashruti":fields.Float(),
        "mashruti motevali":fields.Float(),
        "terms":fields.Float(),
        "TF-gpa":fields.Float(),
        "S-gpa":fields.Float(),
        "Th-gpa":fields.Float(),
    }
)

classifier = joblib.load('gpaPredictClassifier\gpaPredictClassifier.joblib')

@gpaPredict_ns.route('/gpapredict')
class Predict(Resource):
    @gpaPredict_ns.expect(gpaPredict_model)
    def post(self):
        formData=request.get_json()
        data = [val for val in formData.values()]
        predictions = classifier.predict(np.array(data).reshape(1, -1))
        prediction=round(predictions[0],2)

        return prediction
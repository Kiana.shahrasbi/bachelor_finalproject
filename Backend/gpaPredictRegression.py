import joblib as joblib
import numpy as np
from flask_restx import Resource,Namespace,fields
from flask_jwt_extended import JWTManager,create_access_token,create_refresh_token,get_jwt_identity,jwt_required
from flask import Flask, request, json, jsonify,make_response

gpaPredictRegression_ns=Namespace("gpapredict",description="A namespace for prediction")

gpaPredictRegression_model=gpaPredictRegression_ns.model(
    "gpapredict",{
        "F_obtained_units":fields.Integer(),
        "F_pass":fields.Integer(),
        "F_fail":fields.Integer(),
        "TF_gpa":fields.Float(),
        "S_obtained_units":fields.Integer(),
        "S_pass":fields.Integer(),
        "S_fail":fields.Integer(),
        "S_gpa":fields.Float(),
        "TS_gpa":fields.Float(),
        "p_o":fields.Float(),
    }
)


classifier = joblib.load('gpaPredictRegression\ClassifierRegression.joblib')

@gpaPredictRegression_ns.route('/gparegression')
class Predict(Resource):
    @gpaPredictRegression_ns.expect(gpaPredictRegression_model)
    def post(self):
        formData=request.get_json()
        data = [val for val in formData.values()]
        predictions = classifier.predict(np.array(data).reshape(1, -1))
        prediction=round(predictions[0],2)

        return prediction
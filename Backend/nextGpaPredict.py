import joblib
from flask import jsonify,Response
from flask_restx import Resource, Namespace, fields
from model import testnextgpa
import pandas as pd
import numpy as np
from config import dataframe
from sklearn.preprocessing import MinMaxScaler

nextGpaPredict_ns = Namespace("nextgpa", description="A namespace for predicting next gpa")

# model(serializer)
nextGpaPredict_model = nextGpaPredict_ns.model(
    "nextgpa",
    {
        "id":fields.Integer()
    }
)

dataframe['gender'].replace({'Female': '1', 'Male': '0'}, inplace=True)
dataframe['residence'].replace({'Yes': '0', 'No': '1'}, inplace=True)
my_list = []
group_one=[]
group_two=[]
group_three=[]


def marks(mark):
    if (mark >= 17):
        return (1)
    elif (mark >= 12 and mark < 17):
        return (2)
    elif (mark < 12):
        return (3)


dataframe["1_gpa_level"]=dataframe["1_gpa"].apply(marks)
dataframe["1_cgpa_level"]=dataframe["1_cgpa"].apply(marks)
dataframe["2_gpa_level"]=dataframe["2_gpa"].apply(marks)
dataframe["2_cgpa_level"]=dataframe["2_cgpa"].apply(marks)
dataframe["TF_gpa_level"]=dataframe["TF_gpa"].apply(marks)
dataframe["F_gpa_level"]=dataframe["F_gpa"].apply(marks)

classifier = joblib.load('classifierNextGpa\classifierNextGpa.joblib')

for i in range(0,dataframe.shape[0]):
    df = dataframe[[ '1_obtained_units', '1_total_obtained_units',
       '1_passed_units', '1_deleted_units',
       '1_failed_units', '1_gpa', '1_cgpa', '1_mashruti',
       '2_obtained_units', '2_total_obtained_units', '2_passed_units',
       '2_total_passed_units', '2_deleted_units', '2_failed_units', '2_gpa',
       '2_cgpa', '2_mashruti', '3_obtained_units',
       'gender', 'residence', 'birthdate', 'entering','mashrooti',
       'entering_type', 'obtained_units_till_Th', 'f_o','p_o',
       'terms_last', 'm_t', '1_gpa_level', '1_cgpa_level', '2_gpa_level',
       '2_cgpa_level','TF_gpa_level']]

    scaler = MinMaxScaler()
    scaler.fit(df)
    scaler.data_max_
    x = scaler.transform(df)
    d=pd.DataFrame(x)

    data=d.iloc[i].values
    predictions = classifier.predict(np.array(data).reshape(1, -1))
    predicted = round(predictions[0])
    my_list.append(predicted)

dataframe['predicted']=my_list

for i in range(0,dataframe.shape[0]):
    if(dataframe['predicted'].iloc[i]==1):
        group_one.append(dataframe['student_id'].iloc[i])

for i in range(0,dataframe.shape[0]):
    if(dataframe['predicted'].iloc[i]==2):
        group_two.append(dataframe['student_id'].iloc[i])

for i in range(0,dataframe.shape[0]):
    if(dataframe['predicted'].iloc[i]==3):
        group_three.append(dataframe['student_id'].iloc[i])




@nextGpaPredict_ns.route('/gpa_group_one')
class NextGpaPredictResource(Resource):
    def get(self):
        arr1=[]
        for member in group_one:
            arr1.append(int(member))

        return jsonify(arr1)

@nextGpaPredict_ns.route('/gpa_group_two')
class RecipesResource(Resource):
    def get(self):
        arr2 = []
        for member in group_two:
            arr2.append(int(member))
        return jsonify(arr2)

@nextGpaPredict_ns.route('/gpa_group_three')
class RecipesResource(Resource):
    def get(self):
        arr3 = []
        for member in group_three:
            arr3.append(int(member))
        return jsonify(arr3)


@nextGpaPredict_ns.route('/length_group_one')
class RecipesResource(Resource):
    def get(self):
        return jsonify(len(group_one))

@nextGpaPredict_ns.route('/length_group_two')
class RecipesResource(Resource):
    def get(self):
        return jsonify(len(group_two))

@nextGpaPredict_ns.route('/length_group_three')
class RecipesResource(Resource):
    def get(self):
        return jsonify(len(group_three))
from decouple import config
import os
from sqlalchemy import create_engine
import pandas as pd

BASE_DIR=os.path.dirname(os.path.realpath(__file__))
print(BASE_DIR)

class Config:
    SECRET_KEY=config('SECRET_KEY')
    SQLALCHEMY_TRACK_MODIFICATIONS=config('SQLALCHEMY_TRACK_MODIFICATIONS',cast=bool)


class DevConfig(Config):
    SQLALCHEMY_DATABASE_URI='postgresql://postgres:kiana@localhost/project'
    DEBUG=True
    SQLALCHEMY_ECHO=True

class ProdConfig(Config):
    pass

class TestConfig(Config):
    SQLALCHEMY_DATABASE_URI='sqlite:///test.db'
    SQLALCHEMY_ECHO=False
    TESTING=True


engine = create_engine('postgresql://postgres:kiana@localhost/project')
dataframe = pd.read_sql_query('select * from "studying"', con=engine)
dataframe4=pd.read_sql_query('select * from "fourSemester"', con=engine)
dataframe_216=pd.read_sql_query('select * from "216_studying"', con=engine)







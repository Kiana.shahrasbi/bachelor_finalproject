import joblib
from flask import jsonify,Response
from flask_restx import Resource, Namespace, fields
from config import dataframe4
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import pandas as pd


termPredict_ns = Namespace("termPrediction", description="A namespace for predicting term number")

# model(serializer)
termPredict_model = termPredict_ns.model(
    "termPredict",
    {
        "id":fields.Integer(),
        "gender":fields.String(),
        "residence":fields.String(),
        "gpa":fields.Float(),
        "term":fields.Integer()
    }
)

df1=dataframe4.copy()
id=[]
gender=[]
residence=[]
gpa=[]
my_list=[]
terms=[]
over_f=0
over_m=0

gender=df1['gender']
residence=df1['residence']
id=df1['student_id']
gpa=df1['4_cgpa']


dataframe4['gender'].replace({'Female': '1', 'Male': '0'}, inplace=True)
dataframe4['residence'].replace({'Yes': '0', 'No': '1'}, inplace=True)


classifier = joblib.load('termClassifier\classifierTerm.joblib')

for i in range(0,dataframe4.shape[0]):
    df = dataframe4[['1_obtained_units', '1_total_obtained_units',
       '1_passed_units', '1_total_passed_units', '1_deleted_units',
       '1_failed_units', '1_gpa', '1_cgpa', '1_mashruti',
       '2_obtained_units', '2_total_obtained_units', '2_passed_units',
       '2_total_passed_units', '2_deleted_units', '2_failed_units', '2_gpa',
       '2_cgpa', '2_mashruti', '3_obtained_units',
       '3_total_obtained_units', '3_passed_units', '3_total_passed_units',
       '3_deleted_units', '3_failed_units', '3_gpa', '3_cgpa', '3_mashruti', '4_obtained_units', '4_total_obtained_units',
       '4_passed_units', '4_total_passed_units', '4_deleted_units',
       '4_failed_units', '4_gpa', '4_cgpa', '4_mashruti', 'gender', 'residence',
       'birthdate', 'entering', 'mashruti']]

    scaler = MinMaxScaler()
    scaler.fit(df)
    scaler.data_max_
    x = scaler.transform(df)
    d=pd.DataFrame(x)

    data=d.iloc[i].values
    predictions = classifier.predict(np.array(data).reshape(1, -1))
    predicted = round(predictions[0])
    my_list.append(predicted)


dataframe4['predicted']=my_list

temp=dataframe4.copy()
temp['predicted'].replace({1: '<=9', 2: '>=10'}, inplace=True)
terms=temp['predicted']


report1=dataframe4[['predicted','gender']].copy()
below_f=report1[(report1.predicted==1) & (report1.gender == '1')].count()['gender']
below_m=report1[(report1.predicted==1) & (report1.gender == '0')].count()['gender']
over_f=report1[(report1.predicted==2) & (report1.gender == '1')].count()['gender']
over_m=report1[(report1.predicted==2) & (report1.gender == '0')].count()['gender']
total_below=report1[(report1.predicted==1)].count()['predicted']
total_over=report1[(report1.predicted==2)].count()['predicted']



@termPredict_ns.route('/getid')
class RecipesResource(Resource):
    def get(self):
        arr1 = []
        for member in id:
            arr1.append(int(member))
        return jsonify(arr1)

@termPredict_ns.route('/getgender')
class RecipesResource(Resource):
    def get(self):
        arr2 = []
        for member in gender:
            arr2.append(member)
        return jsonify(arr2)

@termPredict_ns.route('/getresidence')
class RecipesResource(Resource):
    def get(self):
        arr3 = []
        for member in residence:
            arr3.append(member)
        return jsonify(arr3)

@termPredict_ns.route('/getgpa')
class RecipesResource(Resource):
    def get(self):
        arr4 = []
        for member in gpa:
            arr4.append(float(member))
        return jsonify(arr4)

@termPredict_ns.route('/getterms')
class RecipesResource(Resource):
    def get(self):
        arr5 = []
        for member in terms:
            arr5.append(member)
        return jsonify(arr5)


@termPredict_ns.route('/below_females')
class RecipesResource(Resource):
    def get(self):
        return jsonify(int(below_f))

@termPredict_ns.route('/below_males')
class RecipesResource(Resource):
    def get(self):
        return jsonify(int(below_m))

@termPredict_ns.route('/over_males')
class RecipesResource(Resource):
    def get(self):
        return jsonify(int(over_m))

@termPredict_ns.route('/over_females')
class RecipesResource(Resource):
    def get(self):
        return jsonify(int(over_f))

@termPredict_ns.route('/total_below')
class RecipesResource(Resource):
    def get(self):
        return jsonify(int(total_below))

@termPredict_ns.route('/total_over')
class RecipesResource(Resource):
    def get(self):
        return jsonify(int(total_over))
from flask import Flask,render_template
from flask_restx import Api
from model import Recipe,User,testnextgpa
from exts import db
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from auth import auth_ns
from flask_cors import CORS
from gpaPredict import gpaPredict_ns
from nextGpaPredict import nextGpaPredict_ns
from gpaPredictRegression import gpaPredictRegression_ns
from mashrutiReport import mashrutiReport_ns
from termPrediction import termPredict_ns
from mainReport import mainReport_ns

def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)
    CORS(app)
    db.init_app(app)
    migrate=Migrate(app,db)
    JWTManager(app)
    api = Api(app, doc='/docs')


    api.add_namespace(auth_ns)
    api.add_namespace(gpaPredict_ns)
    api.add_namespace(nextGpaPredict_ns)
    api.add_namespace(gpaPredictRegression_ns)
    api.add_namespace(mashrutiReport_ns)
    api.add_namespace(termPredict_ns)
    api.add_namespace(mainReport_ns)


    @app.shell_context_processor
    def make_shell_context():
        return {
            "db": db,
            "Recipe": Recipe,
            "user":User,
        }


    return app


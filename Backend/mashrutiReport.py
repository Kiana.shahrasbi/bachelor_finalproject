import joblib
from flask import jsonify,Response
from flask_restx import Resource, Namespace, fields
from config import dataframe


mashrutiReport_ns = Namespace("report", description="A namespace for reporting")

# model(serializer)
mashrutiReport_model = mashrutiReport_ns.model(
    "report_m",
    {
        "id":fields.Integer(),
        "gpa":fields.Float(),
        "mashrooti":fields.Integer()
    }
)
id=[]
mashrooti=[]
gpa=[]

id=dataframe['student_id']
gpa=dataframe['2_cgpa']
mashrooti=dataframe['reported_mashrooti']



@mashrutiReport_ns.route('/mashruti_id')
class RecipesResource(Resource):
    def get(self):
        arr1 = []
        for member in id:
            arr1.append(int(member))
        return jsonify(arr1)

@mashrutiReport_ns.route('/mashruti_gpa')
class RecipesResource(Resource):
    def get(self):
        arr2 = []
        for member in gpa:
            arr2.append(float(member))
        return jsonify(arr2)

@mashrutiReport_ns.route('/mashruti_mashrooti')
class RecipesResource(Resource):
    def get(self):
        arr3 = []
        for member in mashrooti:
            arr3.append(int(member))
        return jsonify(arr3)


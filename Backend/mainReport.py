import joblib
from flask import jsonify,Response
from flask_restx import Resource, Namespace, fields
from config import dataframe_216


mainReport_ns = Namespace("mainreport", description="A namespace for reporting")

# model(serializer)
mainReport_model = mainReport_ns.model(
    "mainreport",
    {

    }
)

enterings=[]
females=[]
males=[]
resi_y=0
resi_no=0
enterings_count=[]
grades=[]
mashruti_count=[]


dataframe_216['gender'].replace({'Female': '1', 'Male': '0'}, inplace=True)
dataframe_216['residence'].replace({'Yes': '0', 'No': '1'}, inplace=True)

#entering report
enterings=dataframe_216.entering.unique()

df = dataframe_216[['gender', 'entering']].copy()


for i in range (0,len(enterings)):
  females.append(df[(df.gender=='1') & (df.entering == enterings[i])].count()['gender'])
  males.append(df[(df.gender=='0') & (df.entering == enterings[i])].count()['gender'])


for i in range(0,enterings.size):
  enterings_count.append(df.loc[df.entering == enterings[i], 'entering'].count())


#residence report
resi_no=dataframe_216.loc[dataframe_216.residence == '1', 'residence'].count()
resi_y=dataframe_216.loc[dataframe_216.residence == '0', 'residence'].count()


#gpa report
def marks(mark):
    if (mark>=19):
        return (1)
    elif (mark >= 18 and mark < 19):
        return (2)
    elif (mark >= 17 and mark < 18):
        return (3)
    elif (mark >= 16 and mark < 17):
        return (4)
    elif (mark >= 15 and mark < 16):
        return (5)
    elif (mark >= 14 and mark < 15):
        return (6)
    elif (mark >= 13 and mark < 14):
        return (7)
    elif (mark >= 12 and mark < 13):
        return (8)
    elif (mark<12):
        return (9)



dataframe_216["gpa_level"]=dataframe_216["gpa"].apply(marks)
for i in range(1,10):
    grades.append(dataframe_216.loc[dataframe_216.gpa_level == i, 'gpa_level'].count())


#mashruti report
mashruti=dataframe_216.mashruti.unique()

for i in range (0,len(mashruti)):
  mashruti_count.append(dataframe_216[(dataframe_216.mashruti==i) ].count()['mashruti'])


@mainReport_ns.route('/getenterings')
class RResource(Resource):
    def get(self):
        arr1 = []
        for member in enterings:
            arr1.append(int(member))
        return jsonify(arr1)


@mainReport_ns.route('/getfemales')
class RResource(Resource):
    def get(self):
        arr2 = []
        for member in females:
            arr2.append(int(member))
        return jsonify(arr2)


@mainReport_ns.route('/getmales')
class RResource(Resource):
    def get(self):
        arr3 = []
        for member in males:
            arr3.append(int(member))
        return jsonify(arr3)


@mainReport_ns.route('/getenterings_count')
class RResource(Resource):
    def get(self):
        arr4 = []
        for member in enterings_count:
            arr4.append(int(member))
        return jsonify(arr4)

@mainReport_ns.route('/getgpa')
class RResource(Resource):
    def get(self):
        arr5 = []
        for member in grades:
            arr5.append(int(member))
        return jsonify(arr5)

@mainReport_ns.route('/getmashruti')
class RResource(Resource):
    def get(self):
        arr6 = []
        for member in mashruti_count:
            arr6.append(int(member))
        return jsonify(arr6)

@mainReport_ns.route('/getresi_y')
class RResource(Resource):
    def get(self):
        return jsonify(int(resi_y))


@mainReport_ns.route('/getresi_no')
class RResource(Resource):
    def get(self):
        return jsonify(int(resi_no))
import { Doughnut } from 'react-chartjs-2';
import { Box, Card, CardContent, useTheme } from '@mui/material';
import {useEffect, useState} from 'react';


const TermPieChart= (props) => {
    const theme = useTheme();
    const [totalBelow,setTotalBelow]=useState(0)
    const [totalOver,setTotalOver]=useState(0)

    const getTotalBelow = () => {
        fetch('/termPrediction/total_below')
            .then(res => res.json())
            .then(data => {
                setTotalBelow(data)
            })

    }
    const getTotalOver = () => {
        fetch('/termPrediction/total_over')
            .then(res => res.json())
            .then(data => {
                setTotalOver(data)
            })

    }

    const data = {
        datasets: [
            {
                data: [totalBelow,totalOver],
                backgroundColor: ['#3d5fce', '#4292c4'],
                borderWidth: 1,
                borderColor: '#FFFFFF',
                hoverBorderColor: '#FFFFFF'
            }
        ],
        labels: ['زیر ۱۰ ترم', 'بالای ۱۰ ترم']
    };

    const options = {
        animation: true,
        cutoutPercentage: 80,
        layout: { padding: 0 },
        legend: {
            display: false
        },
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
            backgroundColor: theme.palette.background.paper,
            bodyFontColor: theme.palette.text.secondary,
            borderColor: theme.palette.divider,
            borderWidth: 1,
            enabled: true,
            footerFontColor: theme.palette.text.secondary,
            intersect: false,
            mode: 'index',
            titleFontColor: theme.palette.text.primary
        }
    };


    useEffect(() => {
        getTotalBelow()
        getTotalOver()
    }, []);

    return (
        <Card {...props}>
            <CardContent>
                <Box
                    sx={{
                        height: 180,
                        position: 'relative'
                    }}
                >
                    <Doughnut
                        data={data}
                        options={options}
                    />
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        pt: 2
                    }}
                >
                </Box>
            </CardContent>
        </Card>
    );
}
export default TermPieChart
import * as React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PeopleIcon from '@mui/icons-material/People';
import BarChartIcon from '@mui/icons-material/BarChart';
import LayersIcon from '@mui/icons-material/Layers';
import AssignmentIcon from '@mui/icons-material/Assignment';
import {Link} from "react-router-dom";
import PieChartIcon from '@mui/icons-material/PieChart';
import InsightsIcon from '@mui/icons-material/Insights';
import StackedLineChartIcon from '@mui/icons-material/StackedLineChart';
import GradingIcon from '@mui/icons-material/Grading';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import {logout} from "../auth";


export const mainListItems = (
    <div >
        <ListItem button component={Link} to="/reportpage">
            <ListItemIcon>
                <BarChartIcon />
            </ListItemIcon>
            <ListItemText primary="بررسی وضعیت کلی" />
        </ListItem>
        <ListItem button component={Link} to="/nextgpa">
            <ListItemIcon>
                <InsightsIcon />
            </ListItemIcon>
            <ListItemText  primary="پیش بینی معدل ترم بعد" />
        </ListItem>
        <ListItem button component={Link} to="/mashrutipage">
            <ListItemIcon>
                <PieChartIcon />
            </ListItemIcon>
            <ListItemText primary="بررسی وضعیت مشروطی" />
        </ListItem>
        <ListItem button component={Link} to="/termpage">
            <ListItemIcon>
                <LayersIcon />
            </ListItemIcon>
            <ListItemText primary="پیش بینی تعداد ترم تحصیلی" />
        </ListItem>
        <ListItem button component={Link} to="/classification">
            <ListItemIcon>
                <GradingIcon />
            </ListItemIcon>
            <ListItemText primary="پیش بینی معدل کل" />
        </ListItem>
        <ListItem button component={Link} to="/gparegression">
            <ListItemIcon>
                <StackedLineChartIcon />
            </ListItemIcon>
            <ListItemText primary="پیش بینی معدل کل" />
        </ListItem>



    </div>
);

export const secondaryListItems = (
    <div>
        <ListItem button component={Link} to="/" onClick={()=>{logout()}}>
            <ListItemIcon>
                <ExitToAppIcon />
            </ListItemIcon>
            <ListItemText  primary="خروج" />
        </ListItem>
        <ListItem button component={Link} to="/signup">
            <ListItemIcon>
                <PersonAddAltIcon />
            </ListItemIcon>
            <ListItemText primary="ایجاد حساب کاربری" />
        </ListItem>
    </div>
);
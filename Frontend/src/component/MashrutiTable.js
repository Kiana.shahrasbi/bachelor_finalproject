import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import ClassificationHeader from "./ClassificationHeader"
import { styled } from '@mui/material/styles';
import { tableCellClasses } from '@mui/material/TableCell';


const useStyles = makeStyles((theme) => ({}));
const MashrutiTable = () => {
    const classes = useStyles();
    const [id, setId] = useState([])
    const [gpa, setGpa] = useState([])
    const [mashruti, setMashruti] = useState([])
    const [size,setSize]=useState(0)
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: "#2E3B55",
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor:"#c7dfee",
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    function createData(name, calories, fat, carbs, protein) {
        return { name, calories, fat, carbs, protein };
    }

    const rows = [];

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const columns = [
        { id: 'id', label: 'شناسه دانشجو', minWidth: 170 },
        { id: 'gpa', label: 'معدل کل', minWidth: 100 },
        {
            id: 'mashrooti',
            label: 'تعداد مشروطی',
            minWidth: 100,
            align: 'right',
        },
    ];

    function prepareData(){
        for (let i = 0; i < id.length; i++) {
            createData(id[i],gpa[i],mashruti[i])
        }
    }

    function createData(id,gpa,mashrooti) {
        rows.push({id,gpa,mashrooti})
    }


    const getInfoGpa = () => {
        fetch('/report/mashruti_gpa')
            .then(res => res.json())
            .then(data => {
                setGpa(data)
            })

    }
    const getInfomashrooti = () => {
        fetch('/report/mashruti_mashrooti')
            .then(res => res.json())
            .then(data => {
                setMashruti(data)
            })

    }
    const getInfoId = () => {
        fetch('/report/mashruti_id')
            .then(res => res.json())
            .then(data => {
                setId(data)
                setSize(data.length)
            })

    }


    prepareData()

    useEffect(() => {
        getInfoGpa()
        getInfomashrooti()
        getInfoId()
    }, []);

    return (
        <div>
            <Paper sx={{ width: '100%', overflow: 'hidden',marginTop:7 }}>
                <TableContainer sx={{ maxHeight: 440 }}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <StyledTableRow>
                                {columns.map((column) => (
                                    <StyledTableCell
                                        key={column.id}
                                        align={column.align}
                                        style={{ minWidth: column.minWidth }}
                                    >
                                        {column.label}
                                    </StyledTableCell>
                                ))}
                            </StyledTableRow>
                        </TableHead>
                        <TableBody>
                            {rows
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row) => {
                                    return (
                                        <StyledTableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                            {columns.map((column) => {
                                                const value = row[column.id];
                                                return (
                                                    <StyledTableCell key={column.id} align={column.align}>
                                                        {column.format && typeof value === 'number'
                                                            ? column.format(value)
                                                            : value}
                                                    </StyledTableCell>
                                                );
                                            })}
                                        </StyledTableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>

        </div>
    )
}
export default MashrutiTable

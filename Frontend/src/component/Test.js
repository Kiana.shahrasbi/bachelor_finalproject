import ReactApexChart from 'react-apexcharts';
import { Box, Card, CardHeader } from '@mui/material';
import {useEffect, useState} from 'react';




const ReportHBarChart=()=> {
    const [males,setMales]=useState([])
    const [females,setFemales]=useState([])
    const [enterings,setEnterings]=useState([])
    const [enteringsCount,setEnteringsCount]=useState([])

    const getEnterings = () => {
        fetch('/mainreport/getenterings')
            .then(res => res.json())
            .then(data => {
                setEnterings(data)
            })

    }

    const getEnteringsCount = () => {
        fetch('/mainreport/getenterings_count')
            .then(res => res.json())
            .then(data => {
                setEnteringsCount(data)
            })

    }

    const getFemales= () => {
        fetch('/mainreport/getfemales')
            .then(res => res.json())
            .then(data => {
                setFemales(data)
            })

    }

    const getMales= () => {
        fetch('/mainreport/getmales')
            .then(res => res.json())
            .then(data => {
                setMales(data)
            })

    }






    const data = {
        datasets: [
            {
                backgroundColor: '#29489f',
                barPercentage: 0.5,
                barThickness: 15,
                borderRadius: 4,
                categoryPercentage: 0.5,
                data: females,
                label: 'خانم ها',
                maxBarThickness: 10
            },
            {
                backgroundColor: '#4c5e83',
                barPercentage: 0.5,
                barThickness: 15,
                borderRadius: 4,
                categoryPercentage: 0.5,
                data: males,
                label: 'آقایان',
                maxBarThickness: 10
            },
            {
                backgroundColor: '#2E3B55',
                barPercentage: 0.5,
                barThickness: 15,
                borderRadius: 4,
                categoryPercentage: 0.5,
                data: enteringsCount,
                label: 'کل دانشجویان',
                maxBarThickness: 10
            }
        ],
        labels: enterings
    };

    const chartOptions = {
        plotOptions: {
            bar: { barHeight: '40%', borderRadius: 2 ,}
        },
        fill: {
            colors: ['#2E3B55']
        },
    };

    useEffect(() => {
        getEnterings()
        getFemales()
        getMales()
        getEnteringsCount()
    }, []);

    return (
        <Card>
            <CardHeader DIR="RTL" style={{color:"#2E3B55",fontSize:"small"}} title="وضعیت معدل دانشجویان" />
            <Box sx={{ mx: 3 }} dir="ltr">
                <ReactApexChart type="bar" series={data} options={chartOptions} height={364} />
            </Box>
        </Card>
    );
}
export default ReportHBarChart
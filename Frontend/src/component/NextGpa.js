import * as React from 'react';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import MuiDrawer from '@mui/material/Drawer';
import Box from '@mui/material/Box';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { mainListItems, secondaryListItems } from './listItems';
import Chart from './Chart';
import Deposits from './Deposits';
import ClassificationHeader from "./ClassificationHeader"
import NextGpaChart from "./NextGpaChart"
import NextGpaList from "./NextGpaList"
import {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import FmdBadIcon from '@mui/icons-material/FmdBad';
import Star from '@mui/icons-material/LocalPolice';
import VerifiedIcon from '@mui/icons-material/Verified';
import ListItemIcon from '@mui/material/ListItemIcon';




const mdTheme = createTheme();
const useStyles = makeStyles((theme) => ({
    list: {
        color: '#2E3B55',
        dir:"rtl"
    },
}));

const NextGpa=()=> {
    const classes = useStyles();
    const [open, setOpen] = React.useState(true);
    const toggleDrawer = () => {
        setOpen(!open);
    };

    return (
        <ThemeProvider theme={mdTheme}>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <Box
                    component="main"
                    sx={{
                        backgroundColor: (theme) =>
                            theme.palette.mode === 'light'
                                ? theme.palette.grey[100]
                                : theme.palette.grey[900],
                        flexGrow: 1,
                        height: '100vh',
                        overflow: 'auto',
                    }}
                >
                    <ClassificationHeader></ClassificationHeader>
                    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                        <Grid container spacing={3}>
                            { Chart }
                            <Grid item xs={12} md={8} lg={9}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 240,
                                    }}
                                >
                                    <NextGpaChart />
                                </Paper>
                            </Grid>
                            { Deposits }
                            <Grid item xs={12} md={4} lg={3}>
                                <Paper
                                    sx={{
                                        p: 2,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        height: 240,
                                    }}
                                >
                                    <div dir="RTL">
                                        <ul style={{paddingRight:0,listStyleType:"none",marginLeft:10,fontWeight:"bold",fontStyle:"oblique",fontSize:15}}>
                                            <ListItemIcon>
                                                <Star style={{marginLeft:10}}/>
                                                <li style={{color:"#2E3B55"}}>گروه اول شامل دانشجویان با معدل بالای ۱۷ می باشد.</li>
                                            </ListItemIcon>

                                            <ListItemIcon>
                                                <VerifiedIcon style={{marginLeft:10}}/>
                                                <li style={{color:"#2E3B55"}}> گروه دوم شامل دانشجویان با معدل بین ۱۲ و ۱۷ می باشد.</li>
                                            </ListItemIcon>
                                            <ListItemIcon>
                                                <FmdBadIcon style={{marginLeft:10}}/>
                                                <li style={{color:"#2E3B55"}}>گروه سوم شامل دانشجویان با معدل زیر ۱۲ می باشد.</li>
                                            </ListItemIcon>

                                        </ul>
                                    </div>
                                </Paper>
                            </Grid>
                            <Grid item xs={12}>
                                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                                    <NextGpaList/>
                                </Paper>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>
            </Box>
        </ThemeProvider>
    );
}
export default NextGpa
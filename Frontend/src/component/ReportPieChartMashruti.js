import { Doughnut } from 'react-chartjs-2';
import { Box, Card, CardContent, CardHeader, Divider, Typography, useTheme } from '@mui/material';
import LaptopMacIcon from '@mui/icons-material/LaptopMac';
import PhoneIcon from '@mui/icons-material/Phone';
import TabletIcon from '@mui/icons-material/Tablet';
import {useEffect, useState} from 'react';


const ReportPieChartResidence= (props) => {
    const theme = useTheme();
    const [mashruti,setMashruti]=useState([])

    const getMashruti = () => {
        fetch('/mainreport/getmashruti')
            .then(res => res.json())
            .then(data => {
                setMashruti(data)
            })

    }


    const data = {
        datasets: [
            {
                data: mashruti,
                backgroundColor: ['#298319', '#2E3B55','#b96e46','#a40e0e'],
                borderWidth: 1,
                borderColor: '#FFFFFF',
                hoverBorderColor: '#FFFFFF'
            }
        ],
        labels: ['۰ مشروطی','۱ مشروطی','۲ مشروطی','۳ مشروطی']
    };

    const options = {
        animation: true,
        cutoutPercentage: 80,
        layout: { padding: 0 },
        legend: {
            display: false
        },
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
            backgroundColor: theme.palette.background.paper,
            bodyFontColor: theme.palette.text.secondary,
            borderColor: theme.palette.divider,
            borderWidth: 1,
            enabled: true,
            footerFontColor: theme.palette.text.secondary,
            intersect: false,
            mode: 'index',
            titleFontColor: theme.palette.text.primary
        }
    };


    useEffect(() => {
        getMashruti()
    }, []);

    return (
        <Card {...props}>
            <CardHeader DIR="RTL" style={{color:"#2E3B55",fontSize:"small"}}
                        title="وضعیت مشروطی دانشجویان"
            />
            <Divider />
            <CardContent>
                <Box
                    sx={{
                        height: 320,
                        position: 'relative'
                    }}
                >
                    <Doughnut
                        data={data}
                        options={options}
                    />
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        pt: 2
                    }}
                >
                </Box>
            </CardContent>
        </Card>
    );
}
export default ReportPieChartResidence
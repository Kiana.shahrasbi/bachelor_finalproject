import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import CommentIcon from '@mui/icons-material/Comment';
import {useEffect, useState} from 'react';
import ListSubheader from '@mui/material/ListSubheader';

export default function CheckboxList() {
    const [checked, setChecked] = React.useState([0]);
    const [groupThree, setGroupThree] = useState([]);

    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };
    const getGroupThree = () => {
        fetch('/nextgpa/gpa_group_three')
            .then(res => res.json())
            .then(data => {
                setGroupThree(data)
            })

    }

    useEffect(() => {
        getGroupThree()
    }, []);

    return (
        <List sx={{
            width: '100%',
            maxWidth: 360,
            bgcolor: '#d9e4ee',
            position: 'relative',
            overflow: 'auto',
            maxHeight: 470,
            marginTop: 7
        }}subheader={<ListSubheader style={{background:"#2E3B55"}} ><span style={{marginLeft:50,color:"white"}}>دانشجویان احتمالی با معدل زیر ۱۲ در ترم آینده</span></ListSubheader>}>

            {groupThree.map((value) => {
                const labelId = `checkbox-list-label-${value}`;

                return (
                    <ListItem
                        key={value}

                        disablePadding
                    >
                        <ListItemButton role={undefined} onClick={handleToggle(value)} dense>
                            <ListItemIcon>
                                <Checkbox
                                    edge="start"
                                    checked={checked.indexOf(value) !== -1}
                                    tabIndex={-1}
                                    disableRipple
                                    inputProps={{ 'aria-labelledby': labelId }}
                                />
                            </ListItemIcon>
                            <ListItemText  style={{fontWeight:900}} id={labelId} primary={`Student ID: ${value}`} />
                        </ListItemButton>
                    </ListItem>
                );
            })}
        </List>
    );
}

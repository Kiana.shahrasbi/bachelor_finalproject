import { Doughnut } from 'react-chartjs-2';
import { Box, Card, CardContent, CardHeader, Divider, Typography, useTheme } from '@mui/material';
import LaptopMacIcon from '@mui/icons-material/LaptopMac';
import PhoneIcon from '@mui/icons-material/Phone';
import TabletIcon from '@mui/icons-material/Tablet';
import {useEffect, useState} from 'react';


const ReportPieChartResidence= (props) => {
    const theme = useTheme();
    const [resi_y,setResi_y]=useState(0)
    const [resi_no,setResi_no]=useState(0)

    const getResi_no = () => {
        fetch('/mainreport/getresi_no')
            .then(res => res.json())
            .then(data => {
                setResi_no(data)
            })

    }

    const getResi_y = () => {
        fetch('/mainreport/getresi_y')
            .then(res => res.json())
            .then(data => {
                setResi_y(data)
            })

    }




    const data = {
        datasets: [
            {
                data: [resi_no,resi_y],
                backgroundColor: ['#1d378f', '#2E3B55'],
                borderWidth: 8,
                borderColor: '#FFFFFF',
                hoverBorderColor: '#FFFFFF'
            }
        ],
        labels: ['دانشجویان غیربومی', 'دانشجویان بومی']
    };

    const options = {
        animation: true,
        cutoutPercentage: 80,
        layout: { padding: 0 },
        legend: {
            display: false
        },
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
            backgroundColor: theme.palette.background.paper,
            bodyFontColor: theme.palette.text.secondary,
            borderColor: theme.palette.divider,
            borderWidth: 1,
            enabled: true,
            footerFontColor: theme.palette.text.secondary,
            intersect: false,
            mode: 'index',
            titleFontColor: theme.palette.text.primary
        }
    };


    useEffect(() => {
        getResi_no()
        getResi_y()
    }, []);

    return (
        <Card {...props}>
            <CardHeader DIR="RTL" style={{color:"#2E3B55",fontSize:"small"}}
                        title="وضعیت بومی بودن دانشجویان"
            />
            <Divider />
            <CardContent>
                <Box
                    sx={{
                        height: 250,
                        position: 'relative'
                    }}
                >
                    <Doughnut
                        data={data}
                        options={options}
                    />
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        pt: 2
                    }}
                >
                </Box>
            </CardContent>
        </Card>
    );
}
export default ReportPieChartResidence
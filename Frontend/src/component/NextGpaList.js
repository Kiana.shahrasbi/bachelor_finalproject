import ListSubheader from '@mui/material/ListSubheader';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import StarBorder from '@mui/icons-material/StarBorder';
import ListItem from '@mui/material/ListItem';
import FmdBadIcon from '@mui/icons-material/FmdBad';
import Star from '@mui/icons-material/LocalPolice';
import VerifiedIcon from '@mui/icons-material/Verified';
import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import ClassificationHeader from "./ClassificationHeader";
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import ListItemText from '@mui/material/ListItemText';
import {FixedSizeList} from 'react-window';

const useStyles = makeStyles((theme) => ({}));
const NextGpaList = () => {
    const classes = useStyles();
    const [open1, setOpen1] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);
    const [open3, setOpen3] = React.useState(false);
    const [groupOne, setGroupOne] = useState([]);
    const [groupTwo, setGroupTwo] = useState([]);
    const [groupThree, setGroupThree] = useState([]);


    const getGroupOne = () => {
        fetch('/nextgpa/gpa_group_one')
            .then(res => res.json())
            .then(data => {
                setGroupOne(data)
            })

    }

    const getGroupTwo = () => {
        fetch('/nextgpa/gpa_group_two')
            .then(res => res.json())
            .then(data => {
                setGroupTwo(data)
            })

    }

    const getGroupThree = () => {
        fetch('/nextgpa/gpa_group_three')
            .then(res => res.json())
            .then(data => {
                setGroupThree(data)
            })

    }

    const listItemsOne = groupOne.map((number) =>

        <li><span style={{color: "#2E3B55"}}>student id:       </span>{number}</li>
    );
    const listItemsTwo = groupTwo.map((number) =>

        <li><span style={{color: "#2E3B55"}}>student id:       </span>{number}</li>
    );

    const listItemsThree = groupThree.map((number) =>

        <li><span style={{color: "#2E3B55"}}>student id:       </span>{number}</li>
    );

    useEffect(() => {
        getGroupOne()
        getGroupTwo()
        getGroupThree()
    }, []);


    const handleClick1 = () => {
        setOpen1(!open1);
    };
    const handleClick2 = () => {
        setOpen2(!open2);
    };
    const handleClick3 = () => {
        setOpen3(!open3);
    };


    return (
        <div>
            <List
                sx={{width: '500%', maxWidth: "100%", bgcolor: 'background.paper'}}
                component="nav"
                aria-labelledby="nested-list-subheader"
                subheader={
                    <ListSubheader component="div" id="nested-list-subheader">

                    </ListSubheader>
                }
            >
                <ListItemButton onClick={handleClick1}>
                    <ListItemIcon>
                        <Star/>
                    </ListItemIcon>
                    <ListItemText primary="group 1"/>
                    {open1 ? <ExpandLess/> : <ExpandMore/>}
                </ListItemButton>
                <Collapse in={open1} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItemButton sx={{pl: 4}}>
                            <List style={{maxWidth: "100%", overflow: 'overflow'}}>
                                {listItemsOne}
                            </List>
                        </ListItemButton>
                    </List>
                </Collapse>
                <ListItemButton onClick={handleClick2}>
                    <ListItemIcon>
                        <VerifiedIcon/>
                    </ListItemIcon>
                    <ListItemText primary="group 2"/>
                    {open2 ? <ExpandLess/> : <ExpandMore/>}
                </ListItemButton>
                <Collapse in={open2} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItemButton sx={{pl: 4}}>
                            <List style={{maxHeight: '100%', maxWidth: "100%", overflow: 'overflow'}}>
                                {listItemsTwo}
                            </List>
                        </ListItemButton>
                    </List>
                </Collapse>
                <ListItemButton onClick={handleClick3}>
                    <ListItemIcon>
                        <FmdBadIcon/>
                    </ListItemIcon>
                    <ListItemText primary="group 3"/>
                    {open3 ? <ExpandLess/> : <ExpandMore/>}
                </ListItemButton>
                <Collapse in={open3} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItemButton sx={{pl: 4}}>
                            <List style={{maxHeight: '100%', maxWidth: "100%", overflow: 'overflow'}}>
                                {listItemsThree}
                            </List>
                        </ListItemButton>
                    </List>
                </Collapse>
            </List>
        </div>
    )
        ;
}

export default NextGpaList

import React from 'react'
import {makeStyles} from "@material-ui/core/styles";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Collapse} from "@material-ui/core";
import {Link} from "react-router-dom";


const useStyles=makeStyles({
    root:{
        width: "200px",
        margin:'50px',
        background:'rgba(0,0,0,0.5)',
    },
    media:{
        width:"200px",
        height:"200px",
    },
    title:{
        fontFamily:'Nunito',
        color:'#9ec2e8',
        direction:'rtl',
        fontSize:"0.9 rem",
    },
    button:{
        direction:'rtl',
        fontSize:"1rem",
        marginLeft:"100px"
    }
});

export default function ImageCardHome({information,checked}){
    const classes=useStyles();
    return(
        <Collapse in={checked} {...(checked ? { timeout: 1000 } : {})}>
        <Card  className={classes.root}>
            <CardActionArea>
                <CardMedia className={classes.media} image={information.imageUrl}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h1" className={classes.title}>
                        {information.title}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button component={Link} to={information.href} className={classes.button}  variant="outlined" size="large" style={{ background: '#9ec2e8' }}>
                    ورود
                </Button>
            </CardActions>
        </Card>
        </Collapse>
    );
}

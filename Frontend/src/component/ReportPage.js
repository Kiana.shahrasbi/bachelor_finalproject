import {Box, Grid, Container, Typography} from '@mui/material';
import ReportBarChart from "./ReportBarChart"
import ReportPieChartResidene from "./ReportPieChartResidence"
import ClassificationHeder from "./ClassificationHeader"
import ReportHBarChart from "./ReportHBarChart"
import ReportPieChartMashruti from "./ReportPieChartMashruti"



const ReportPage = () => {
    return (
        <div>
            <ClassificationHeder/>

            <Container maxWidth="xl">

                <Grid container spacing={3}>

                    <Grid item xs={12} md={6} lg={8} style={{marginTop:50}}>
                        <ReportBarChart/>
                    </Grid>

                    <Grid item xs={12} md={6} lg={4} style={{marginTop:50}}>
                        <ReportPieChartResidene />
                    </Grid>

                    <Grid item xs={12} md={6} lg={8}>
                        <ReportHBarChart/>
                    </Grid>

                    <Grid item xs={12} md={6} lg={4}>
                        <ReportPieChartMashruti/>
                    </Grid>

                    <Grid item xs={12} md={6} lg={4}>
                    </Grid>

                    <Grid item xs={12} md={6} lg={8}>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}
export default ReportPage
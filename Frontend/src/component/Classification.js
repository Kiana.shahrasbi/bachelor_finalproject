import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import ClassificationHeader from "./ClassificationHeader";
import {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {useForm} from 'react-hook-form'
import Alert from '@mui/material/Alert';
import IconButton from "@mui/material/IconButton";
import CloseIcon from '@mui/icons-material/Close';

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: '100vh',
        backgroundImage: `url(${process.env.PUBLIC_URL + '/images/background3.png'})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    r:{
        dir:"rtl"
    }
}));


const Classification = () => {
    const classes = useStyles();
    const {register, handleSubmit, reset, formState: {errors}} = useForm()
    const [serverResponse, setServerResponse] = useState('')
    const [show, setShow] = useState(false)



    const submitForm = (data) => {


        const requestOptions = {
            method: "POST",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }

        fetch('/prediction/gpapredict', requestOptions).then(res => res.json())
            .then(data => {
                console.log(data)
                setServerResponse(data)
                console.log(serverResponse)
                setShow(true)
            })
            .catch(err => console.log(err))
        reset()

    }


    return (
        <div className={classes.root}>
            <ClassificationHeader></ClassificationHeader>
            <Container component="main" maxWidth="xs">

                <CssBaseline/>
                <Box
                    sx={{
                        marginTop: 10,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',

                    }}
                >
                    <Typography component="h1" variant="h5">
                    </Typography>
                    <div >
                        <Box component="form" noValidate sx={{mt: 1}}>
                            <Grid container spacing={4}>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        {...register("pass units", {
                                            required: true,
                                        })}
                                        autoComplete="pass units"
                                        name="pass units"
                                        required
                                        fullWidth
                                        id="pass units"
                                        label="passed units"
                                        autoFocus
                                        variant="standard"
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        {...register("fail units", {
                                            required: true,
                                        })}
                                        required
                                        fullWidth
                                        id="fail units"
                                        label="failed units"
                                        name="fail units"
                                        autoComplete="fail units"
                                        variant="standard"
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        {...register("mashruti", {
                                            required: true,
                                        })}
                                        required
                                        fullWidth
                                        id="mashruti"
                                        label="GPAs below 12"
                                        name="mashruti"
                                        autoComplete="mashruti"
                                        variant="standard"
                                    />
                                    <div dir="rtl" style={{color: "red"}}>
                                        {errors.mashruti && <p>تعداد مشروطی را وارد نمایید.</p>}
                                    </div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        {...register("mashruti motevali", {
                                            required: true,
                                        })}
                                        required
                                        fullWidth
                                        name="mashruti motevali"
                                        label="Continuous GPAs below 12"
                                        id="mashruti motevali"
                                        autoComplete="mashruti motevali"
                                        variant="standard"
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        {...register("terms", {
                                            required: true,
                                        })}
                                        required
                                        fullWidth
                                        name="terms"
                                        label="terms"
                                        id="terms"
                                        autoComplete="terms"
                                        variant="standard"
                                    />
                                    <div dir="rtl" style={{color: "red"}}>
                                        {errors.terms && <p>تعداد ترم گذرانده را وارد نمایید.</p>}
                                    </div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        {...register("TF-gpa", {
                                            required: true,
                                        })}
                                        required
                                        fullWidth
                                        name="TF-gpa"
                                        label="First GPA"
                                        id="TF-gpa"
                                        autoComplete="TF-gpa"
                                        variant="standard"
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        {...register("S-gpa", {
                                            required: true,
                                        })}
                                        autoComplete="S-gpa"
                                        name="S-gpa"
                                        required
                                        fullWidth
                                        id="S-gpa"
                                        label="Second GPA"
                                        variant="standard"
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        {...register("Th-gpa", {
                                            required: true,
                                        })}
                                        autoComplete="Th-gpa"
                                        name="Th-gpa"
                                        required
                                        fullWidth
                                        id="Th-gpa"
                                        label="Last GPA"
                                        variant="standard"
                                    />
                                </Grid>
                            </Grid>
                            <Button onClick={handleSubmit(submitForm)} style={{background: '#2E3B55'}}
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    sx={{mt: 6, mb: 2}}
                            >
                                مشاهده نتیجه
                            </Button>
                            <Grid container justifyContent="flex-end">
                            </Grid>
                            <div >
                                {show ?
                                    <>
                                        <Alert variant="filled" severity="success" style={{backgroundColor: "#2E3B55"}}
                                               action={
                                                   <IconButton
                                                       aria-label="close"
                                                       color="inherit"
                                                       size="small"
                                                       onClick={() => {
                                                           setShow(false);
                                                       }}
                                                   >
                                                       <CloseIcon fontSize="inherit"/>
                                                   </IconButton>
                                               }
                                               sx={{mb: 2}}
                                        >
                                            <p> your predicted Cgpa is: {serverResponse}</p>

                                        </Alert>
                                    </>
                                    :
                                    <></>
                                }
                            </div>
                        </Box>
                    </div>
                </Box>
            </Container>

        </div>
    );
}
export default Classification
import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {AppBar, Collapse, IconButton, Toolbar} from "@material-ui/core";
import MainHeader from "./MainHeader"
import ImageCardMain from "./ImageCardMain";
import informationsMain from "../static/informationsMain";


const useStyles = makeStyles((theme) => ({
    mainroot: {
        maxWidth: '100%',
        overflowX: 'hidden',
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        overflow:"auto"
    },
    root: {
        maxWidth: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minheight: '100vh',
        fontFamily: 'Nunito',
        overflow: "auto",
    },
    appbar: {
        background: 'none',
        fontFamily: 'Nunito',
    },
    colorText: {
        color: "#9ec2e8",
    },
    title: {
        color: "#fff",
        fontSize: "3.5rem",
        direction: 'rtl',
    },
    container: {
        textAlign: "center",
    },
    options: {
        minHeight: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        [theme.breakpoints.down("md")]: {
            flexDirection: "column"
        },
        overflow: "auto"
    },
}));
const MainPage= () =>  {
    const classes = useStyles();
    const [checked, setChecked] = useState(false);
    useEffect(() => {
        setChecked(true);
    }, []);
    return (
        <div className={classes.mainroot}>
            <div className={classes.root}>
                <AppBar className={classes.appbar} >
                    <MainHeader></MainHeader>
                    <Toolbar className={classes.appbarWrapper}>
                    </Toolbar>

                </AppBar>
                <Collapse in={checked}
                          {...(checked ? { timeout: 2000 } : {})}
                          collapsedHeight={50}>
                    <div className={classes.container}>
                        <div className={classes.options}>
                            <ImageCardMain information={informationsMain[2]}/>
                            <ImageCardMain information={informationsMain[1]}/>
                            <ImageCardMain information={informationsMain[0]}/>
                        </div>
                        <div className={classes.options}>
                            <ImageCardMain information={informationsMain[3]}/>
                            <ImageCardMain information={informationsMain[4]}/>
                            <ImageCardMain information={informationsMain[5]}/>
                        </div>
                    </div>
                </Collapse>
            </div>

        </div>
    )
}
export default MainPage

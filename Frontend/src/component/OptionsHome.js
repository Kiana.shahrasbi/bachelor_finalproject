import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import ImageCardHome from "./ImageCardHome";
import informationsHome from "../static/informationsHome"
import useWindowPosition from "../hook/useWindowPosition";

const useStyles=makeStyles((theme)=>({
    root:{
        minHeight: '100vh',
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        [theme.breakpoints.down("md")]:{
            flexDirection:"column",
        },
        overflow:"auto"
    }
}));
export default function (){
    const classes=useStyles();
    const checked=useWindowPosition('header');
    return(
        <div className={classes.root} id="options-page">
            <ImageCardHome information={informationsHome[1]} checked={checked}/>
            <ImageCardHome information={informationsHome[0]} checked={checked}/>
        </div>
    )
}
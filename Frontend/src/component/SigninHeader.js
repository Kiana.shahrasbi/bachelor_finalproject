import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import {makeStyles} from "@material-ui/core/styles";
import {Menu, MenuItem} from "@mui/material";
import {Link} from 'react-router-dom'
import {logout} from '../auth'

const useStyles = makeStyles({
    button: {
        direction: 'rtl',
    },
});

export default function ButtonAppBar() {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <Box sx={{flexGrow: 1}}>
            <AppBar style={{background: '#2E3B55'}} >
                <Toolbar>
                    <IconButton id="basic-button"
                                aria-controls="basic-menu"
                                aria-haspopup="true"
                                aria-expanded={open ? 'true' : undefined}
                                onClick={handleClick}
                                size="large"
                                edge="start"
                                color="inherit"
                                aria-label="menu"
                                sx={{mr: 2}}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        MenuListProps={{
                            'aria-labelledby': 'basic-button',
                        }}
                    >
                        <Link to="/signup" style={{textDecoration:"none",color:'#2E3B55'}}>
                            <MenuItem
                                onClick={()=>{logout()}}
                                dir="rtl"
                            > ایجاد حساب کاربری </MenuItem>
                        </Link>
                        <Link to="/" style={{textDecoration:"none",color:'#2E3B55'}}>
                            <MenuItem
                                dir="rtl"
                                onClick={()=>{logout()}}
                            > خروج </MenuItem>
                        </Link>
                    </Menu>
                    <Typography variant="h6" component="div" sx={{flexGrow: 1}}>

                    </Typography>
                    <Button onClick={()=>{logout()}} component={Link} to="/"className={classes.button} variant="text" size="large" color="inherit">صفحه اصلی</Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
}

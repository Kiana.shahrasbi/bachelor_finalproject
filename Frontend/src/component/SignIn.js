import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import SigninHeader from './SigninHeader';
import logo from "./user.png"
import {Link} from "react-router-dom";
import {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {useForm} from 'react-hook-form'
import {login} from '../auth'
import {useNavigate} from "react-router-dom";
import {Alert, AlertTitle} from "@mui/material";
import Grid from '@mui/material/Grid';




const useStyles = makeStyles((theme) => ({
    link: {
        color: '#2E3B55',
        fontSize: "large",
        direction: "rtl",
        textDecoration: "none",
        display: "flex",
        marginLeft: "200px"
    },
    root: {
        maxWidth: '100%',
        display: 'flex',
        flexDirection: 'column',
        marginLeft: "10px",
        overflowY:"hidden"
    },
    pic:{
        marginTop:"40px"
    }
}));


const SignIn = () => {

    const {register, handleSubmit, reset, formState: {errors}} = useForm()
    const [lock, setLock] = useState(false)
    const history = useNavigate()

    const classes = useStyles();


    const loginUser = (data) => {

        const requestOptions = {
            method: "POST",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }

        fetch('/auth/login', requestOptions)
            .then(res => res.json())
            .then(data => {
                    console.log(data)
                    if (data.access_token) {
                        setLock(false)
                        console.log(data.access_token)
                        login(data.access_token)
                        history("/mainpage")
                    } else {
                        setLock(true)
                    }
                }
            )


        reset()
    }
    return (
        <div className={classes.root}>
            {lock ?
                <>
                    <SigninHeader></SigninHeader>
                    <div dir="rtl">
                        <Alert style={{marginTop: "70px",marginRight:"20px"}} variant="filled" severity="error">
                            <AlertTitle>خطا</AlertTitle>
                            <strong>نام کاربری و یا رمز عبور اشتباه است.</strong>
                        </Alert>
                    </div>
                </>
                :
                <SigninHeader></SigninHeader>
            }
            <Container component="main" maxWidth="xs" >
                <Grid sx={{ flexGrow: 1 }}container spacing={2}>
                <CssBaseline/>
                <Box
                    sx={{
                        marginTop: 7,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <img className={classes.pic} src={logo} width="30%" height="30%"/>
                    <Box component="form" noValidate sx={{mt: 3}} >
                        <Grid item xs={12} >
                        <TextField
                            {...register("username", {
                                required: true,
                                maxLength: 25,
                            })}
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="username"
                            autoComplete="username"
                            autoFocus
                        />
                        <div dir="rtl" style={{color: "red"}}>
                            {errors.username && <p>نام کاربری خود را به درستی وارد نمایید.</p>}
                        </div>
                        </Grid>
                        <Grid item xs={12} >
                        <TextField
                            {...register("password", {
                                required: true,
                                minLength: 6,
                            })}
                            margin="normal"
                            required
                            fullWidth
                            label="password"
                            type="password"
                            id="password"
                            autoComplete="password"
                        />
                        <div dir="rtl" style={{color: "red"}}>
                            {errors.password && <p>گذرواژه خود را به درستی وارد نمایید.</p>}
                        </div>
                        </Grid>
                        <Button onClick={handleSubmit(loginUser)} style={{background: '#2E3B55'}}
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{mt: 3, mb: 2}}
                        >
                            ورود
                        </Button>

                        <Link to="/signup" className={classes.link}> ایجاد حساب کاربری </Link>
                    </Box>
                </Box>
                </Grid>
            </Container>
        </div>
    );
}
export default SignIn


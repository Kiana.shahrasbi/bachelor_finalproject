import ReactApexChart from 'react-apexcharts';
import {Box, Card, CardHeader} from '@mui/material';
import {useEffect, useState} from 'react';


const ReportHBarChart = () => {
    const [gpa, setGpa] = useState([])

    const getGpa = () => {
        fetch('/mainreport/getgpa')
            .then(res => res.json())
            .then(data => {
                setGpa(data)
            })

    }

    const CHART_DATA = [{data: gpa}];

    const chartOptions = {
        plotOptions: {
            bar: {horizontal: true, barHeight: '40%', borderRadius: 5},
        },
        fill: {
            colors: ['#1d378f']
        },
        chart: {
            toolbar: {
                show: false
            },
        },
        xaxis: {
            categories: [
                '19-20',
                '18-19',
                '17-18',
                '16-17',
                '15-16',
                '14-15',
                '13-14',
                '12-13',
                '<12',

            ]
        },

    };

    useEffect(() => {
        getGpa()
    }, []);


    return (
        <Card>
            <CardHeader DIR="RTL" style={{color: "#2E3B55", fontSize: "small"}} title="وضعیت معدل دانشجویان"/>
            <Box sx={{mx: 3}} dir="ltr">
                <ReactApexChart type="bar" series={CHART_DATA} options={chartOptions} height={364}/>
            </Box>
        </Card>
    );
}
export default ReportHBarChart
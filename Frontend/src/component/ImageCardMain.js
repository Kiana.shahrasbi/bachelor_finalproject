import React from 'react'
import {makeStyles} from "@material-ui/core/styles";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Collapse} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles=makeStyles({
    root:{
        border:' 5px solid #2E3B55',
        width:"250px",
        margin:'30px',
        backgroundColor:'#85acd2',
        marginTop:'150px',
        marginLeft: "40px",
    },
    media:{
        height:200,
        width:250,
    },
    title:{
        marginLeft:"100px",
        fontFamily:'Nunito',
        color:'#2E3B55',
        direction:'rtl',
        fontSize:"medium",
        fontWeight:"bold",
        textAlign: "right",
        whiteSpace:"nowrap"
    },
    button:{
        direction:'rtl',
        fontSize:"1.5rem",
        color:'#b6dede',

    },
    discription:{
        fontSize:"medium",
        textAlign:"right"
    }
});

export default function ImageCardMain({information}){
    const classes=useStyles();
    return(
        <Card className={classes.root}>
            <CardActionArea >
                <CardMedia className={classes.media} image={information.imageUrl}
                />
                <CardContent>
                    <Typography  variant="h5" component="h1" className={classes.title}>
                        {information.title}
                    </Typography>
                    <br/>
                    <Typography  className={classes.discription}>
                        {information.description}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button component={Link} to={information.href}  fullWidth  className={classes.button}  variant="outlined" size="small" style={{ background: '#2E3B55' }}>
                    اجرا
                </Button>
            </CardActions>
        </Card>
    );
}

import { Chart, registerables } from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { Box, Button, Card, CardContent, CardHeader, Divider, useTheme } from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import {useEffect, useState} from 'react';
Chart.register(...registerables);

const ReportBarChart = (props) => {
    const theme = useTheme();
    const [males,setMales]=useState([])
    const [females,setFemales]=useState([])
    const [enterings,setEnterings]=useState([])
    const [enteringsCount,setEnteringsCount]=useState([])

    const getEnterings = () => {
        fetch('/mainreport/getenterings')
            .then(res => res.json())
            .then(data => {
                setEnterings(data)
            })

    }

    const getEnteringsCount = () => {
        fetch('/mainreport/getenterings_count')
            .then(res => res.json())
            .then(data => {
                setEnteringsCount(data)
            })

    }

    const getFemales= () => {
        fetch('/mainreport/getfemales')
            .then(res => res.json())
            .then(data => {
                setFemales(data)
            })

    }

    const getMales= () => {
        fetch('/mainreport/getmales')
            .then(res => res.json())
            .then(data => {
                setMales(data)
            })

    }




    useEffect(() => {
        getEnterings()
        getFemales()
        getMales()
        getEnteringsCount()
    }, []);

    const data = {
        datasets: [
            {
                backgroundColor: '#8c1c25',
                barPercentage: 0.5,
                barThickness: 15,
                borderRadius: 4,
                categoryPercentage: 0.5,
                data: females,
                label: 'خانم ها',
                maxBarThickness: 10
            },
            {
                backgroundColor: '#29489f',
                barPercentage: 0.5,
                barThickness: 15,
                borderRadius: 4,
                categoryPercentage: 0.5,
                data: males,
                label: 'آقایان',
                maxBarThickness: 10
            },
            {
                backgroundColor: '#2E3B55',
                barPercentage: 0.5,
                barThickness: 15,
                borderRadius: 4,
                categoryPercentage: 0.5,
                data: enteringsCount,
                label: 'کل دانشجویان',
                maxBarThickness: 10
            }
        ],
        labels: enterings
    };

    const options = {
        animation: true,
        cornerRadius: 20,
        layout: { padding: 0 },
        legend: { display: false },
        maintainAspectRatio: false,
        responsive: true,
        xAxes: [
            {
                ticks: {
                    fontColor: theme.palette.text.secondary
                },
                gridLines: {
                    display: true,
                    drawBorder: true
                }
            }
        ],
        yAxes: [
            {
                ticks: {
                    fontColor: theme.palette.text.secondary,
                    beginAtZero: true,
                    min: 0
                },
                gridLines: {
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: theme.palette.divider,
                    drawBorder: false,
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                    zeroLineColor: theme.palette.divider
                }
            }
        ],
        tooltips: {
            backgroundColor: theme.palette.background.paper,
            bodyFontColor: theme.palette.text.secondary,
            borderColor: theme.palette.divider,
            borderWidth: 1,
            enabled: true,
            footerFontColor: theme.palette.text.secondary,
            intersect: false,
            mode: 'index',
            titleFontColor: theme.palette.text.primary
        }
    };

    return (
        <Card {...props}>
            <CardHeader DIR="RTL" style={{color:"#2E3B55"}}
                title="دانشجویان ورودی های مختلف"
            />
            <Divider />
            <CardContent>
                <Box
                    sx={{
                        height: 240,
                        position: 'relative'
                    }}
                >
                    <Bar
                        data={data}
                        options={options}
                    />
                </Box>
            </CardContent>
            <Divider />
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    p: 2
                }}
            >

            </Box>
        </Card>
    );
}
export default ReportBarChart
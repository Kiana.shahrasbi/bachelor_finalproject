import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import SignUpHeader from './SignUpHeader';
import logoSignup from "./user1.png";
import {Link} from "react-router-dom";
import {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {useForm} from 'react-hook-form'
import Alert from '@mui/material/Alert';
import IconButton from "@mui/material/IconButton";
import CloseIcon from '@mui/icons-material/Close';
import  { useRef } from "react";


const useStyles = makeStyles((theme) => ({
    link: {
        color: '#2E3B55',
        fontSize: "large",
        direction: "rtl",
        textDecoration: "none",
        display: "flex",
        marginLeft: "200px",
    },
    root: {
        maxWidth: '100%',
        display: 'flex',
        flexDirection: 'column',
        overflowY: "hidden",
        marginLeft: "10px"
    },
    pic: {
        marginTop: "50px",
    },
}));


const SignUp = () => {
    const classes = useStyles();
    const {register, handleSubmit, reset, formState: {errors}} = useForm()
    const [open, setOpen] = useState(false)
    const [error, setError] = useState(false)
    const [serverResponse, setServerResponse] = useState('')


    const submitForm = (data) => {

        if (data.password === data.confirmPassword) {
            const body = {
                email: data.email,
                username: data.username,
                password: data.password
            }

            const requestOptions = {
                method: "POST",
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(body)
            }

            fetch('/auth/signup', requestOptions).then(res => res.json())
                .then(data => {
                    console.log(data)
                    setServerResponse(data.message)
                    console.log(serverResponse)
                    setOpen(true)
                })
                .catch(err => console.log(err))

            reset()
        }
        else {
            setError(true)
        }
    }

    return (
        <div className={classes.root}>
            {open ?
                <>
                    <SignUpHeader></SignUpHeader>
                    <Alert style={{marginTop: "70px", marginRight: "20px"}} severity="info"
                           action={
                               <IconButton
                                   aria-label="close"
                                   color="inherit"
                                   size="small"
                                   onClick={() => {
                                       setOpen(false);
                                   }}
                               >
                                   <CloseIcon fontSize="inherit"/>
                               </IconButton>
                           }
                           sx={{mb: 2}}
                    >
                        {serverResponse}
                    </Alert>
                </>
                :
                <SignUpHeader></SignUpHeader>
            }
            <Container component="main" maxWidth="xs">
                <Grid sx={{flexGrow: 1}} container spacing={1}>
                    <CssBaseline/>
                    <Box
                        sx={{
                            marginTop: 7,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',

                        }}
                    >
                        <img className={classes.pic} src={logoSignup} width="30%" height="30%"/>
                        <Box component="form" noValidate sx={{mt: 3}}>
                            <Grid item xs={12}>
                                <TextField
                                    {...register("email", {
                                        required: true,
                                        maxLength: 80,
                                        pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                    })}
                                    margin="dense"
                                    required
                                    fullWidth
                                    id="email"
                                    label="email address"
                                    autoComplete="email"
                                />
                                <div dir="rtl" style={{color: "red"}}>
                                    {errors.email && <p>آدرس ایمیل خود را به درستی وارد نمایید.(حداکثر ۸۰ کاراکتر)</p>}
                                </div>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    {...register("username", {
                                        required: true,
                                        maxLength: 25,
                                    })}
                                    margin="dense"
                                    required
                                    fullWidth
                                    id="username"
                                    label="username"
                                    autoComplete="username"
                                />
                                <div dir="rtl" style={{color: "red"}}>
                                    {errors.username &&
                                    <p>نام کاربری خود را به درستی وارد نمایید.(حداکثر ۲۵ کاراکتر)</p>}
                                </div>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField

                                    {...register("password", {
                                            required: true,
                                            minLength:6,
                                    })}

                                    name="password"
                                    margin="dense"
                                    required
                                    fullWidth
                                    label="password"
                                    type="password"
                                    id="password"
                                    autoComplete="password"
                                />

                                <div dir="rtl" style={{color: "red"}}>
                                    {errors.password && <p>گذرواژه ای حداقل به طول ۶ وارد نمایید.</p>}
                                </div>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    {...register("confirmPassword", {
                                            required: true,
                                            minLength: 6,
                                        })}
                                    margin="dense"
                                    required
                                    fullWidth
                                    label="Confirm password"
                                    type="password"
                                    id="confirmPassword"
                                    autoComplete="confirmPassword"
                                />
                                <div>
                                    {error ?
                                        <>
                                            <Alert style={{marginTop: "10px"}} severity="warning"
                                                   action={
                                                       <IconButton
                                                           aria-label="close"
                                                           color="inherit"
                                                           size="small"
                                                           onClick={() => {
                                                               setError(false);
                                                           }}
                                                       >
                                                           <CloseIcon fontSize="inherit"/>
                                                       </IconButton>
                                                   }
                                                   sx={{mb: 1}}
                                            >
                                                <p dir="rtl" style={{color: "black",marginLeft:"20px"}}>گذرواژه را به درستی تکرار نکرده اید!</p>
                                            </Alert>
                                        </>
                                        :
                                        <></>
                                    }
                                </div>

                                <div dir="rtl" style={{color: "red"}}>
                                    {errors.confirmPassword && <p>تکرار گذرواژه خود را به درستی وارد نمایید.</p>}
                                </div>
                            </Grid>
                            <Button onClick={handleSubmit(submitForm)} style={{background: '#2E3B55'}}
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    sx={{mt: 1, mb: 2}}
                            >
                                ایجاد حساب کاربری
                            </Button>
                            <Link to="/signin" className={classes.link}> ورود به حساب کاربری </Link>
                        </Box>
                    </Box>
                </Grid>
            </Container>
        </div>
    );
}
export default SignUp
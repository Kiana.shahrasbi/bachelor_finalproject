import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import {useEffect, useState} from 'react';
import { styled } from '@mui/material/styles';
import { tableCellClasses } from '@mui/material/TableCell';


const TermList = () => {
    const [id, setId] = useState([])
    const [gpa, setGpa] = useState([])
    const [terms, setTerms] = useState([])
    const [gender, setGender] = useState([])
    const [residence, setResidence] = useState([])
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: "#2E3B55",
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor:"#c7dfee",
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    const rows = [];

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const columns = [
        { id: 'id', label: 'شناسه دانشجو', minWidth: 170 },
        { id: 'gender', label: 'جنسیت', minWidth: 170 },
        { id: 'residence', label: 'وضعیت بومی بودن', minWidth: 170 },
        { id: 'gpa', label: 'آخرین معدل', minWidth: 100 },
        {
            id: 'terms',
            label: 'پیش بینی تعداد ترم اتمام تحصیلات',
            minWidth: 100,
            align: 'right',
        },
    ];

    function prepareData(){
        for (let i = 0; i < id.length; i++) {
            createData(id[i],gender[i],residence[i],gpa[i],terms[i])
        }
    }

    function createData(id,gender,residence,gpa,terms) {
        rows.push({id,gender,residence,gpa,terms})
    }

    const getInfoID = () => {
        fetch('/termPrediction/getid')
            .then(res => res.json())
            .then(data => {
                setId(data)
            })

    }

    const getInfoGender = () => {
        fetch('/termPrediction/getgender')
            .then(res => res.json())
            .then(data => {
                setGender(data)
            })

    }

    const getInfoResidence = () => {
        fetch('/termPrediction/getresidence')
            .then(res => res.json())
            .then(data => {
                setResidence(data)
            })

    }


    const getInfoGpa = () => {
        fetch('/termPrediction/getgpa')
            .then(res => res.json())
            .then(data => {
                setGpa(data)
            })

    }
    const getInfoTerms = () => {
        fetch('/termPrediction/getterms')
            .then(res => res.json())
            .then(data => {
                setTerms(data)
            })

    }



    prepareData()

    useEffect(() => {
        getInfoID()
        getInfoGender()
        getInfoResidence()
        getInfoGpa()
        getInfoTerms()
    }, []);

    return (
        <div>
            <Paper sx={{ width: '100%', overflow: 'hidden',marginTop:7 }}>
                <TableContainer sx={{ maxHeight: 440 }}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <StyledTableRow>
                                {columns.map((column) => (
                                    <StyledTableCell
                                        key={column.id}
                                        align={column.align}
                                        style={{ minWidth: column.minWidth }}
                                    >
                                        {column.label}
                                    </StyledTableCell>
                                ))}
                            </StyledTableRow>
                        </TableHead>
                        <TableBody>
                            {rows
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row) => {
                                    return (
                                        <StyledTableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                            {columns.map((column) => {
                                                const value = row[column.id];
                                                return (
                                                    <StyledTableCell key={column.id} align={column.align}>
                                                        {column.format && typeof value === 'number'
                                                            ? column.format(value)
                                                            : value}
                                                    </StyledTableCell>
                                                );
                                            })}
                                        </StyledTableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>

        </div>
    )
}
export default TermList

import * as React from 'react';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import {useForm} from 'react-hook-form'

const ThirdFormReg=()=> {
    const {register, handleSubmit, reset, formState: {errors}} = useForm()
    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12} md={6}>
                    <TextField
                        {...register("TS_gpa", {
                            required: true,
                        })}
                        required
                        id="TS_gpa"
                        label="previous cgpa"
                        fullWidth
                        autoComplete="TS_gpa"
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        {...register("p_o", {
                            required: true,
                        })}
                        required
                        id="p_o"
                        label="passed units ratio"
                        fullWidth
                        autoComplete="p_o"
                    />
                </Grid>
            </Grid>
        </React.Fragment>
    );
}
export default ThirdFormReg
import { Chart, registerables } from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { Box, Card, CardContent, Divider, useTheme } from '@mui/material';
import {useEffect, useState} from 'react';
Chart.register(...registerables);

const TermBarChart = (props) => {
    const theme = useTheme();
    const [below_f,setBelow_f]=useState(0)
    const [below_m,setBelow_m]=useState(0)
    const [over_f,setOver_f]=useState(0)
    const [over_m,setOver_m]=useState(0)

    const getBelowMales = () => {
        fetch('/termPrediction/below_males')
            .then(res => res.json())
            .then(data => {
                setBelow_m(data)
            })

    }

    const getBelowFemales= () => {
        fetch('/termPrediction/below_females')
            .then(res => res.json())
            .then(data => {
                setBelow_f(data)
            })

    }

    const getOverFemales= () => {
        fetch('/termPrediction/over_females')
            .then(res => res.json())
            .then(data => {
                setOver_f(data)
            })

    }

    const getOverMales= () => {
        fetch('/termPrediction/over_males')
            .then(res => res.json())
            .then(data => {
                setOver_m(data)
            })

    }

    useEffect(() => {
        getBelowFemales()
        getBelowMales()
        getOverFemales()
        getOverMales()
    }, []);

    const data = {
        datasets: [
            {
                backgroundColor: '#c7dfee',
                barPercentage: 0.5,
                barThickness: 60,
                borderRadius: 4,
                categoryPercentage: 0.5,
                data: [below_f, over_f],
                label: 'خانم ها',
                maxBarThickness: 50
            },
            {
                backgroundColor: '#2E3B55',
                barPercentage: 0.5,
                barThickness: 60,
                borderRadius: 4,
                categoryPercentage: 0.5,
                data: [below_m,over_m],
                label: 'آقایان',
                maxBarThickness: 50
            }
        ],
        labels: ['زیر ۱۰ ترم','بالای ۱۰ ترم']
    };

    const options = {
        animation: true,
        cornerRadius: 20,
        layout: { padding: 0 },
        legend: { display: false },
        maintainAspectRatio: false,
        responsive: true,
        xAxes: [
            {
                ticks: {
                    fontColor: theme.palette.text.secondary
                },
                gridLines: {
                    display: false,
                    drawBorder: false
                }
            }
        ],
        yAxes: [
            {
                ticks: {
                    fontColor: theme.palette.text.secondary,
                    beginAtZero: true,
                    min: 0
                },
                gridLines: {
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: theme.palette.divider,
                    drawBorder: false,
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                    zeroLineColor: theme.palette.divider
                }
            }
        ],
        tooltips: {
            backgroundColor: theme.palette.background.paper,
            bodyFontColor: theme.palette.text.secondary,
            borderColor: theme.palette.divider,
            borderWidth: 1,
            enabled: true,
            footerFontColor: theme.palette.text.secondary,
            intersect: false,
            mode: 'index',
            titleFontColor: theme.palette.text.primary
        }
    };

    return (
        <Card {...props}>
            <Divider />
            <CardContent>
                <Box
                    sx={{
                        height: 190,
                        position: 'relative'
                    }}
                >
                    <Bar
                        data={data}
                        options={options}
                    />
                </Box>
            </CardContent>
            <Divider />
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    p: 2
                }}
            >

            </Box>
        </Card>
    );
}
export default TermBarChart
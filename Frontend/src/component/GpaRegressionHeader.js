import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import {makeStyles} from "@material-ui/core/styles";
import {Menu, MenuItem} from "@mui/material";
import {Link} from "react-router-dom";

export default function ButtonAppBar() {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <Box >
            <AppBar style={{background: '#2E3B55',width:"100%",padding:"0"}} >
                <Toolbar>
                    <IconButton id="basic-button"
                                aria-controls="basic-menu"
                                aria-haspopup="true"
                                aria-expanded={open ? 'true' : undefined}
                                onClick={handleClick}
                                size="large"
                                edge="start"
                                color="inherit"
                                aria-label="menu"
                                sx={{mr: 2}}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        MenuListProps={{
                            'aria-labelledby': 'basic-button',
                        }}
                    >
                        <Link to="/mainpage" style={{textDecoration: "none", color: '#2E3B55'}}>
                            <MenuItem
                                dir="rtl"
                                onClick={handleClose}
                            > صفحه ی تحلیل ها </MenuItem></Link>
                        <Link to="/" style={{textDecoration: "none", color: '#2E3B55'}}>
                            <MenuItem
                                dir="rtl"
                                onClick={handleClose}
                            > خروج </MenuItem>
                        </Link>
                    </Menu>
                    <Typography variant="h6" component="div" sx={{flexGrow: 1}}>

                    </Typography>
                    <Button component={Link} to="/mainpage" variant="text" size="large" color="inherit">بازگشت</Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
}

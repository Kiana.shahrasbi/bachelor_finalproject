import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import Header from "./GpaRegressionHeader"
import {Box, Container, Grid, Typography, CardActions} from '@mui/material';
import {useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {
    Button,
    Card,
    CardContent,
    CardHeader,
    Divider,
    TextField
} from '@mui/material';
import {useForm} from 'react-hook-form'
import Alert from '@mui/material/Alert';
import IconButton from "@mui/material/IconButton";
import CloseIcon from '@mui/icons-material/Close';


const theme = createTheme();
const useStyles = makeStyles((theme) => ({
    subColor: {
        color: "#31abc9",
        fontWeight: "bold",
    },
    alertt: {
        marginTop: 20,
        marginRight: 385
    }
}));
const GpaRegression = () => {
    const classes = useStyles();
    const {register, handleSubmit, reset, formState: {errors}} = useForm()
    const [serverResponse, setServerResponse] = useState('')
    const [show, setShow] = useState(false)
    const submitForm = (data) => {


        const requestOptions = {
            method: "POST",
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }

        fetch('/gpapredict/gparegression', requestOptions).then(res => res.json())
            .then(data => {
                console.log(data)
                setServerResponse(data)
                console.log(serverResponse)
                setShow(true)
            })
            .catch(err => console.log(err))
        reset()

    }

    return (
        <ThemeProvider theme={theme}>
            <div>
                <Header/>
                <Grid container component="main" sx={{height: '100vh'}} style={{marginTop:50}}>

                    <CssBaseline/>

                    <Grid
                        item
                        xs={12}
                        sm={8}
                        md={5}
                        sx={{
                            backgroundImage: `url(${process.env.PUBLIC_URL + '/images/background1.jpg'})`,
                            backgroundRepeat: 'no-repeat',
                            backgroundColor: (t) =>
                                t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                        }}
                    />
                    <Grid item xs={false} sm={4} md={7} component={Paper} elevation={6} square>
                        <Box
                            sx={{
                                my: 8,
                                mx: 4,
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center',
                            }}
                        >
                            <Card>
                                <CardHeader dir="RTL" style={{backgroundColor: "#2E3B55"}}
                                            subheader={<Typography className={classes.subColor}>مشخصات ترم
                                                اول</Typography>}
                                />
                                <Divider/>
                                <CardContent>
                                    <Grid
                                        container
                                        spacing={3}
                                    >
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("F_obtained_units", {
                                                    required: true,
                                                })}
                                                autoComplete="F_obtained_units"
                                                name="F_obtained_units"
                                                required
                                                id="F_obtained_units"
                                                label="Obtained units"
                                                autoFocus
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("F_pass", {
                                                    required: true,
                                                })}
                                                autoComplete="F_pass"
                                                name="F_pass"
                                                required
                                                id="F_pass"
                                                label="number of passed units"
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("F_fail", {
                                                    required: true,
                                                })}
                                                autoComplete="F_fail"
                                                name="F_fail"
                                                required
                                                id="F_fail"
                                                label="number of failed units"
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("F_gpa", {
                                                    required: true,
                                                })}
                                                autoComplete="F_gpa"
                                                name="F_gpa"
                                                required
                                                id="F_gpa"
                                                label="gpa"
                                                variant="outlined"
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <Divider/>
                                <CardHeader style={{backgroundColor: "#2E3B55"}} dir="RTL"
                                            subheader={<Typography className={classes.subColor}>مشخصات ترم
                                                دوم</Typography>}
                                />
                                <Divider/>
                                <CardContent>
                                    <Grid
                                        container
                                        spacing={3}
                                    >
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("S_obtained_units", {
                                                    required: true,
                                                })}
                                                autoComplete="S_obtained_units"
                                                name="S_obtained_units"
                                                required
                                                id="S_obtained_units"
                                                label="Obtained units"
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("S_pass", {
                                                    required: true,
                                                })}
                                                autoComplete="S_pass"
                                                name="S_pass"
                                                required
                                                id="S_pass"
                                                label="number of passed units"
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("S_fail", {
                                                    required: true,
                                                })}
                                                autoComplete="S_fail"
                                                name="S_fail"
                                                required
                                                id="S_fail"
                                                label="number of failed units"
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("S_gpa", {
                                                    required: true,
                                                })}
                                                autoComplete="S_gpa"
                                                name="S_gpa"
                                                required
                                                id="S_gpa"
                                                label="gpa"
                                                variant="outlined"
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <Divider/>
                                <CardContent>
                                    <Grid
                                        container
                                        spacing={3}
                                    >
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("TS_gpa", {
                                                    required: true,
                                                })}
                                                autoComplete="TS_gpa"
                                                name="TS_gpa"
                                                required
                                                id="TS_gpa"
                                                label="last cgpa"
                                                variant="outlined"
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            md={6}
                                            xs={12}
                                        >
                                            <TextField
                                                {...register("p_o", {
                                                    required: true,
                                                })}
                                                autoComplete="p_o"
                                                name="p_o"
                                                required
                                                id="p_o"
                                                label="passed units ratio"
                                                variant="outlined"
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <Divider/>
                                <Box style={{backgroundColor: "#2E3B55"}}
                                     sx={{
                                         display: 'flex',
                                         justifyContent: 'flex-end',
                                         p: 2
                                     }}
                                >
                                    <Button style={{background: "#31abc9"}} onClick={handleSubmit(submitForm)}
                                            color="primary"
                                            variant="contained"
                                    >
                                        مشاهده ی نتیجه
                                    </Button>

                                </Box>

                            </Card>
                            <div className={classes.alertt}>
                                {show ?
                                    <>
                                        <Alert variant="filled" severity="success" style={{backgroundColor: "#2E3B55"}}
                                               action={
                                                   <IconButton
                                                       aria-label="close"
                                                       color="inherit"
                                                       size="small"
                                                       onClick={() => {
                                                           setShow(false);
                                                       }}
                                                   >
                                                       <CloseIcon fontSize="inherit"/>
                                                   </IconButton>
                                               }
                                               sx={{mb: 2}}
                                        >
                                            <p> your predicted Cgpa is: {serverResponse}</p>

                                        </Alert>
                                    </>
                                    :
                                    <></>
                                }
                            </div>
                        </Box>
                    </Grid>
                </Grid>
            </div>
        </ThemeProvider>
    );
}
export default GpaRegression
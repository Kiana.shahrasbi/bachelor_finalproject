import { Box, Grid, Container, Typography } from '@mui/material';
import MashrutiTable from "./MashrutiTable"
import Header from "./ClassificationHeader"
import React, {useEffect, useState} from 'react';
import MashrutiList from "./MashrutiList"

const MashrutiPage=()=> {
    return (
        <div>
            <Header></Header>
            <Container maxWidth="xl">

                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6} md={3}>

                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>

                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>

                    </Grid>

                    <Grid item xs={12} md={6} lg={8}>
                        <MashrutiTable/>
                    </Grid>

                    <Grid item xs={12} md={6} lg={4}>
                        <MashrutiList/>
                    </Grid>

                    <Grid item xs={12} md={6} lg={8}>

                    </Grid>

                    <Grid item xs={12} md={6} lg={4}>

                    </Grid>

                    <Grid item xs={12} md={6} lg={8}>

                    </Grid>

                    <Grid item xs={12} md={6} lg={4}>

                    </Grid>

                    <Grid item xs={12} md={6} lg={4}>

                    </Grid>

                    <Grid item xs={12} md={6} lg={8}>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}
export default MashrutiPage
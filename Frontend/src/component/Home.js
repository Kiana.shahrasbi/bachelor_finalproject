import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {CssBaseline} from "@material-ui/core";
import Options from "./OptionsHome"
import HomeHeader from "./HomeHeader";

const useStyle=makeStyles((theme)=>({
    root: {
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        maxWidth:"100%",
        backgroundImage: `url(${process.env.PUBLIC_URL + '/images/background1.jpg'})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        overflow:"auto"
    },
}));
export default function (){
    const classes=useStyle();
    return(
        <div className={classes.root}>
            <CssBaseline/>
            <HomeHeader></HomeHeader>
            <Options></Options>
        </div>
    );
}
import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import { BarChart, Bar,Tooltip,Legend,CartesianGrid, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import {useEffect, useState} from 'react';



export default function Chart() {
    const theme = useTheme();
    const [sizeGroupOne, setSizeGroupOne] = useState(0);
    const [sizeGroupTwo, setSizeGroupTwo] = useState(0);
    const [sizeGroupThree, setSizeGroupThree] = useState(0);

    const getSizeOne = () => {
        fetch('/nextgpa/length_group_one')
            .then(res => res.json())
            .then(data => {
                setSizeGroupOne(data)
            })
    }
    const getSizeTwo = () => {
        fetch('/nextgpa/length_group_two')
            .then(res => res.json())
            .then(data => {
                setSizeGroupTwo(data)
            })

    }
    const getSizeThree = () => {
        fetch('/nextgpa/length_group_three')
            .then(res => res.json())
            .then(data => {
                setSizeGroupThree(data)
            })

    }
    useEffect(() => {
        getSizeOne()
        getSizeTwo()
        getSizeThree()
    }, []);
    const data = [
        {
            "name": "group 1",
            "member": sizeGroupOne,
        },
        {
            "name": "group 2",
            "member": sizeGroupTwo,
        },
        {
            "name": "group 3",
            "member": sizeGroupThree,
        },
    ]
    return (
        <React.Fragment>
            <ResponsiveContainer>
                <BarChart width={730} height={200} data={data}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="member" fill="#2E3B55" />
                </BarChart>
            </ResponsiveContainer>
        </React.Fragment>
    );
}
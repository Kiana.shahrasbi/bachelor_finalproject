import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {AppBar, Collapse, IconButton, Toolbar} from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {Link as Scroll} from 'react-scroll'

const useStyles = makeStyles((theme) => ({
    root: {
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Nunito',
        display: 'flex',
        minHeight: "100vh",
        overflow: "auto"
    },
    appbar: {
        background: 'none',
        fontFamily: 'Nunito',
    },
    appbarWrapper: {
        width: '80%',
        margin: '0 auto'
    },
    colorText: {
        color: "#9ec2e8",
    },
    title: {
        color: "#fff",
        fontSize: "4.5vw",
        direction: 'rtl',
        display: 'flex',
        flexDirection: 'column',
    },
    container: {
        textAlign: "center",
    },
    goDown: {
        color: "#9ec2e8",
        fontSize: "4vw",
    },
}));
export default function HomePageHeader() {
    const classes = useStyles();
    const [checked, setChecked] = useState(false);
    useEffect(() => {
        setChecked(true);
    }, []);
    return (
        <div className={classes.root} id="header">
            <AppBar className={classes.appbar} elevation={0}>
                <Toolbar className={classes.appbarWrapper}>
                </Toolbar>
            </AppBar>
            <Collapse in={checked}
                      {...(checked ? {timeout: 1000} : {})}
                      collapsedHeight={50}>
                <div className={classes.container}>
                    <h1 className={classes.title}>
                        به <span className={classes.colorText}> سامانه ی تحلیل داده های آموزشی </span> خوش آمدید
                    </h1>
                    <Scroll to="options-page" smooth={true}>
                        <IconButton>
                            <ExpandMoreIcon className={classes.goDown}></ExpandMoreIcon>
                        </IconButton>
                    </Scroll>
                </div>
            </Collapse>
        </div>
    )
}

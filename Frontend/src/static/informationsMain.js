const informationsMain=[
    {
        title: 'بررسی کلی وضعیت دانشجویان',
        imageUrl: process.env.PUBLIC_URL + '/images/13.jpg',
        direction: 'right',
        description: 'بررسی وضعیت دانشجویان مشغول به تحصیل',
        time: 1500,
        href:'/reportpage'
    },
    {
        title: 'پیش بینی معدل ترم بعد دانشجویان',
        imageUrl: process.env.PUBLIC_URL + '/images/3.png',
        direction: 'right',
        description: 'پیش بینی و دسته بندی معدل ترم بعد دانشجویان در قالب سه گروه',
        time: 1500,
        href:'/nextgpa'
    },

    {
        title: 'بررسی وضعیت مشروطی',
        imageUrl: process.env.PUBLIC_URL + '/images/14.png',
        direction: 'right',
        description: 'بررسی وضعیت و تعداد مشروطی دانشجویان',
        time: 1500,
        href:'/mashrutipage'
    },


    {
        title: 'پیش بینی معدل کل دانشجویان',
        imageUrl: process.env.PUBLIC_URL + '/images/15.jpg',
        direction: 'right',
        description: 'پیش بینی معدل کل با استفاده از مشخصات دو ترم',
        time: 1500,
        href:'/gparegression'
    },
    {
        title:'پیش بینی معدل کل دانشجویان',
        imageUrl:process.env.PUBLIC_URL + '/images/1.png',
        direction:'left',
        description:'پیش بینی معدل کل با استفاده از معدل سه ترم',
        time:1500,
        href:'/classification'
    },
    {
        title: 'پیش بینی تعداد ترم تحصیلی',
        imageUrl: process.env.PUBLIC_URL + '/images/11.png',
        direction: 'right',
        description: 'پیش بینی تعداد ترم تحصیلی گذرانده توسط دانشجویان برای اتمام تحصیلات',
        time: 1500,
        href:'/termpage'
    },



];
export default informationsMain;
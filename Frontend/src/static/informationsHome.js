const informationsHome=[
    {
        title:'ورود به سامانه',
        imageUrl:process.env.PUBLIC_URL + '/images/signin.png',
        direction:'left',
        time:1500,
        href:'./signin'
    },
    {
        title: 'ایجاد حساب کاربری',
        imageUrl: process.env.PUBLIC_URL + '/images/images.png',
        direction: 'right',
        time: 1500,
        href:'./signup'
    },
];
export default informationsHome;
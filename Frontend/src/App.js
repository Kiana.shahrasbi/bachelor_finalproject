import './App.css';
import React, {Component} from "react";
import SignIn from "./component/SignIn";
import SignUp from "./component/SignUp"
import MainPage from "./component/MainPage"
import Classification from "./component/Classification"
import Home from "./component/Home"
import {logout, useAuth} from "./auth";
import { Navigate } from 'react-router-dom';
import {BrowserRouter, BrowserRouter as Router, Redirect, Route, Routes} from "react-router-dom";
import NextGpa from "./component/NextGpa";
import GpaRegression from "./component/GpaRegression"
import Mashrutipage from "./component/MashrutiPage"
import TermsPage from "./component/Term"
import ReportPage from "./component/ReportPage"






const APP = () => {
    const [logged] = useAuth();

    return (


        <BrowserRouter>
            <Routes>
                {!logged && (
                    <>
                        <Route path="/"  exact element={<Home/>}/>
                        <Route path="/signup" exact element={<SignUp/>}/>
                        <Route path="/signin" exact element={<SignIn/>}/>





                    </>
                )}
                {logged && (
                    <>
                        <Route path="/" exact element={<Home/>}/>
                        <Route path="/signup" exact element={<SignUp/>}/>
                        <Route path="/signin" exact element={<SignIn/>}/>
                        <Route path="/classification" exact element={<Classification/>}></Route>
                        <Route path="/mainpage" exact element={<MainPage/>}></Route>
                        <Route path="/nextgpa" exact element={<NextGpa/>}></Route>
                        <Route path="/gparegression" exact element={<GpaRegression/>}></Route>
                        <Route path="/mashrutipage" exact element={<Mashrutipage/>}></Route>
                        <Route path="/termpage" exact element={<TermsPage/>}></Route>
                        <Route path="/reportpage" exact element={<ReportPage/>}></Route>




                    </>
                )}
            </Routes>
        </BrowserRouter>
    );
}
export default APP
//<Route path="*" element={<Navigate to ="/" />}/>